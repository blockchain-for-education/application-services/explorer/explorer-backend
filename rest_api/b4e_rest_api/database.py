# Copyright 2018 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ------------------------------------------------------------------------------

import asyncio
import logging

import pymongo
from pymongo import MongoClient
from collections import defaultdict

import datetime

from config.config import MongoDBConfig
from config.constant import MongoConstant

LATEST_BLOCK_NUM = """
SELECT max(block_num) FROM blocks
"""
LOGGER = logging.getLogger(__name__)


class Database(object):
    """Manages connection to the postgres database and makes async queries
    """

    def __init__(self):
        self.mongo = None
        self.b4e_db = None
        self.b4e_block_collection = None
        self.b4e_transaction_collection = None
        self.b4e_transaction_family_collection = None

    def connect(self, host=MongoDBConfig.HOST, port=MongoDBConfig.PORT, user_name=MongoDBConfig.USER_NAME,
                password=MongoDBConfig.PASSWORD):
        if (user_name != "" and password != ""):
            url = f"mongodb://{user_name}:{password}@{host}:{port}"
            self.mongo: MongoClient = MongoClient(url)
        else:
            self.mongo: MongoClient = MongoClient(host=host, port=int(port))
        self.create_collections()

        self._create_index()

    def create_collections(self):
        self.b4e_db = self.mongo[MongoDBConfig.DATABASE]
        self.b4e_block_collection = self.b4e_db[MongoDBConfig.BLOCK_COLLECTION]
        self.b4e_transaction_collection = self.b4e_db[MongoDBConfig.TRANSACTION_COLLECTION]
        self.b4e_transaction_family_collection = self.b4e_db[MongoDBConfig.TRANSACTION_FAMILY_COLLECTION]
        self.b4e_statistic_collection = self.b4e_db[MongoDBConfig.STATISTICS_COLLECTION]

    def _create_index(self):

        LOGGER.info("Indexing mongo db")
        if MongoConstant.tx_id_index not in self.b4e_transaction_collection.index_information():
            self.b4e_transaction_collection.create_index(
                [("transaction_id", "hashed")],
                name=MongoConstant.tx_id_index)
        if MongoConstant.tx_block_number_index not in self.b4e_transaction_collection.index_information():
            self.b4e_transaction_collection.create_index(
                [("block_num", 1)],
                name=MongoConstant.tx_block_number_index)
        if MongoConstant.block_id_index not in self.b4e_block_collection.index_information():
            self.b4e_block_collection.create_index(
                [("block_id", "hashed")],
                name=MongoConstant.block_id_index)
        if MongoConstant.block_num_index not in self.b4e_block_collection.index_information():
            self.b4e_block_collection.create_index(
                [("block_num", 1)],
                name=MongoConstant.block_num_index)
        LOGGER.info("Indexed!!!")

    def disconnect(self):
        self.mongo.close()

    def commit(self):
        pass

    def rollback(self):
        pass

    async def create_auth_entry(self,
                                public_key,
                                encrypted_private_key,
                                hashed_password):
        pass

    async def fetch_agent_resource(self, public_key):
        pass

    async def fetch_all_agent_resources(self):
        pass

    async def fetch_auth_resource(self, public_key):
        pass

    async def fetch_record_resource(self, record_id):
        pass

    async def fetch_all_record_resources(self):
        pass

    def get_transaction(self, transaction_id):
        key = {'_id': transaction_id}
        transaction = self.b4e_transaction_collection.find_one(key)
        LOGGER.info("transaction", transaction)
        return transaction

    def get_transaction_num(self):
        key = {"_id": "statistics"}
        res = self.b4e_statistic_collection.find_one(key) or {}
        num_txs = res.get("number_of_txs") or 0
        return num_txs

    def get_block_num(self):
        key = {"_id": "statistics"}
        res = self.b4e_statistic_collection.find_one(key) or {}
        num_blocks = res.get("number_of_blocks") or 0
        return num_blocks

    def get_family_num(self):
        return self.b4e_transaction_family_collection.count_documents({})

    def get_transaction_family_num(self, family_name):
        key = {'family_name': family_name}
        res = self.b4e_transaction_family_collection.find_one(key) or {}
        total_transaction = res.get("transaction_number") or 0
        return total_transaction

    def get_families(self):
        families = []
        res = self.b4e_transaction_family_collection.find()
        for family in res:
            del family['_id']
            family["total_transaction"] = family.get("transaction_number") or 0
            families.append(family)
        return families

    def get_all_trans(self, limit=1000):
        try:
            limit = int(limit) or 1000
        except:
            limit = 1000
        transactions = []
        res = self.b4e_transaction_collection.find().sort([('block_num', pymongo.DESCENDING)]).limit(limit)
        for transaction in res:
            del transaction['_id']
            transactions.append(transaction)
        return transactions


def timestamp_to_datetime(timestamp):
    return datetime.datetime.fromtimestamp(timestamp)


def to_time_stamp(date_time):
    return datetime.datetime.timestamp(date_time)
